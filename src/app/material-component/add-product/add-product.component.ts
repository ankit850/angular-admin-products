import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  userData: any = {
    product_name : "",
    delivery_charge : null,
    quantity_in : "",
    display_name: "",
  };
  
  constructor(private router : Router, private route : ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      if(params.sendObj){
        this.userData = JSON.parse(params.sendObj)
        console.log("====userData====",this.userData);
      }
    });
   }

  ngOnInit() {
  }

}
