import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent {
  name : any
  obj: any;
  constructor(private router: Router, private route : ActivatedRoute) {}

 


 
  


  productdata: any = [
    {
      "product_name": "Mango",
      "product_type": "BULK_FRUITS_AND_VEGETABLES",
      "display_name": "Potato",
      "buy_price_per_quantity": 30,
      "quantity_in": "KG",
      "product_image_url": "",
      "delivery_charge": 10,
      "express_delivery_available": true,
      "product_selling_quantities": [
      {
      "quantity": 6,
      "price": 180
      }
      ]
    },
    {
      "product_name": "Orange",
      "product_type": "BULK_FRUITS_AND_VEGETABLES",
      "display_name": "Potato",
      "buy_price_per_quantity": 30,
      "quantity_in": "KG",
      "product_image_url": "",
      "delivery_charge": 10,
      "express_delivery_available": true,
      "product_selling_quantities": [
      {
      "quantity": 6,
      "price": 180
      }
      ]
    },
    {
      "product_name": "Apple",
      "product_type": "BULK_FRUITS_AND_VEGETABLES",
      "display_name": "Potato",
      "buy_price_per_quantity": 30,
      "quantity_in": "KG",
      "product_image_url": "",
      "delivery_charge": 10,
      "express_delivery_available": true,
      "product_selling_quantities": [
      {
      "quantity": 6,
      "price": 180
      }
      ]
    }

    

      
  ];

  remove(x){
     this.obj = x;
     let n  = this.productdata.indexOf(this.obj);
    //  console.log(n)
    this.productdata.splice(n,1);
  }
  edit(obj:any){
    this.router.navigate(['add'], {queryParams : {sendObj : JSON.stringify(obj)} } );
  }
  
  add(){
    this.router.navigate(['add'])
  }
  
 
}
